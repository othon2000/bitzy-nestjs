import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { ShortUrl } from './entities/short-url.entity';

export class ShortUrlMapper {
  static dtoToEntity(dto: CreateShortUrlDto): ShortUrl {
    const now = new Date();
    return new ShortUrl(dto.alias as string, new URL(dto.url), now, now);
  }
}
