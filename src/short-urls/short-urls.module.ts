import { Module } from '@nestjs/common';
import { ShortUrlsService } from './short-urls.service';
import { ShortUrlsController } from './short-urls.controller';
import { ShortUrlsInMemRepository } from './repository/short-utls.repository.mem';
import { IShortUrlsRepository } from './repository/short-utls.repository';

@Module({
  controllers: [ShortUrlsController],
  providers: [
    { provide: IShortUrlsRepository, useClass: ShortUrlsInMemRepository },
    ShortUrlsService,
  ],
  exports: [ShortUrlsService],
})
export class ShortUrlsModule {}
