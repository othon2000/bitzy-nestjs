import { Injectable } from '@nestjs/common';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { UpdateShortUrlDto } from './dto/update-short-url.dto';
import { ShortUrl } from './entities/short-url.entity';
import { IShortUrlsRepository } from './repository/short-utls.repository';
import { ShortUrlMapper } from './short-urls.mapper';

@Injectable()
export class ShortUrlsService {
  constructor(private readonly repository: IShortUrlsRepository) {}

  private generateAlias(): string {
    let outString = '';
    const inOptions = 'abcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 8; i++) {
      outString += inOptions.charAt(
        Math.floor(Math.random() * inOptions.length),
      );
    }
    return outString;
  }

  async create(dto: CreateShortUrlDto): Promise<string | undefined> {
    if (!dto.alias) {
      dto.alias = this.generateAlias();
    }
    const alreadyExists = await this.findOne(dto.alias);
    if (!alreadyExists) {
      const entity = ShortUrlMapper.dtoToEntity(dto);
      this.repository.save(entity);
      return dto.alias;
    } else {
      return undefined;
    }
  }

  async findAll(): Promise<ShortUrl[]> {
    return this.repository.findAll();
  }

  async findOne(alias: string): Promise<ShortUrl | undefined> {
    return this.repository.get(alias);
  }

  async update(alias: string, dto: UpdateShortUrlDto): Promise<boolean> {
    const previous = await this.findOne(alias);
    if (previous) {
      if (dto.url) {
        previous.url = new URL(dto.url);
        previous.updatedAt = new Date();
        this.repository.save(previous); // with the current in-mem implementation this is not really required
      }
      return true;
    } else {
      return false;
    }
  }

  async remove(alias: string): Promise<boolean> {
    return this.repository.delete(alias);
  }

  async findOneAndIncreaseHits(alias: string): Promise<ShortUrl | undefined> {
    const shortUrl = await this.findOne(alias);
    if (shortUrl) {
      shortUrl.hits = shortUrl.hits + 1;
      this.repository.save(shortUrl);
      return shortUrl;
    } else {
      return undefined;
    }
  }
}
