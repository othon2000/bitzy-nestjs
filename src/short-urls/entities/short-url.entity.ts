import { ApiProperty } from '@nestjs/swagger';

export class ShortUrl {
  //fields
  @ApiProperty()
  alias: string;
  @ApiProperty({ type: 'string', example: 'http://example.com/' })
  url: URL;
  @ApiProperty({ type: 'number' })
  hits = 0;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;

  constructor(alias: string, url: URL, createdAt: Date, updatedAt: Date) {
    this.alias = alias;
    this.url = url;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
