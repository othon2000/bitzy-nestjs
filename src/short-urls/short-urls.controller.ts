import {
  Controller,
  Get,
  Post,
  Res,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { ShortUrlsService } from './short-urls.service';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { UpdateShortUrlDto } from './dto/update-short-url.dto';
import { Response } from 'express';
import { ShortUrl } from './entities/short-url.entity';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiQuery,
} from '@nestjs/swagger';

enum SortField {
  hits = 'hits',
  alias = 'alias',
  url = 'url',
  created = 'created',
  updated = 'updated',
}
class SortFieldUtil {
  static valueOf(str: string) {
    return SortField[str as keyof typeof SortField];
  }
}

enum SortOrder {
  asc = 'asc',
  desc = 'desc',
}
class SortOrderUtil {
  static valueOf(str: string) {
    return SortOrder[str as keyof typeof SortOrder];
  }
}
@Controller('admin/short-urls')
export class ShortUrlsController {
  constructor(private readonly shortUrlsService: ShortUrlsService) {}

  @Post()
  @ApiBody({ type: CreateShortUrlDto })
  @ApiCreatedResponse({ description: 'short URL created' })
  @ApiConflictResponse({ description: 'short URL already exists' })
  @ApiBadRequestResponse({ description: 'validation errors' })
  async create(
    @Body() createShortUrlDto: CreateShortUrlDto,
    @Res() res: Response,
  ) {
    const alias = await this.shortUrlsService.create(createShortUrlDto);
    if (alias) {
      res.status(HttpStatus.CREATED).json(alias).send();
    } else {
      res
        .status(HttpStatus.CONFLICT)
        .json(`${createShortUrlDto.alias} already exists`)
        .send();
    }
  }

  @Get()
  @ApiQuery({ name: 'sort', enum: SortField, required: false })
  @ApiQuery({ name: 'order', enum: SortOrder, required: false })
  async findAll(
    @Query('sort') sortBy?: string,
    @Query('order') sortOrder?: string,
  ) {
    const urls = await this.shortUrlsService.findAll();
    if (sortBy != undefined && SortFieldUtil.valueOf(sortBy)) {
      const by = sortBy as SortField;
      const order: SortOrder =
        sortOrder && SortOrderUtil.valueOf(sortOrder)
          ? (sortOrder as SortOrder)
          : SortOrder.asc;
      urls.sort((a, b) => this.compare(a, b, by, order));
    }
    return urls;
  }

  @Get(':alias')
  @ApiOkResponse({ type: ShortUrl })
  @ApiNotFoundResponse({
    description: 'when a short URL with the alias does not exist',
  })
  async findOne(@Param('alias') alias: string, @Res() res: Response) {
    const one = await this.shortUrlsService.findOne(alias);
    if (one) {
      res.status(HttpStatus.OK).json(one).send();
    } else {
      res.status(HttpStatus.NOT_FOUND).json(`${alias} not found`).send();
    }
  }

  @Patch(':alias')
  @ApiOkResponse({ description: 'short URL modified' })
  @ApiBadRequestResponse({ description: 'validation errors' })
  @ApiNotFoundResponse({
    description: 'when a short URL with the alias does not exist',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        url: {
          type: 'string',
          example: 'http://newurl.com/',
        },
      },
    },
  })
  async update(
    @Param('alias') alias: string,
    @Body() updateShortUrlDto: UpdateShortUrlDto,
    @Res() res: Response,
  ) {
    if (await this.shortUrlsService.update(alias, updateShortUrlDto)) {
      res.status(HttpStatus.OK).send();
    } else {
      res.status(HttpStatus.NOT_FOUND).json(`${alias} not found`).send();
    }
  }

  @Delete(':alias')
  @ApiCreatedResponse({ description: 'short URL deleted' })
  @ApiNotFoundResponse({
    description: 'when a short URL with the alias does not exist',
  })
  async remove(@Param('alias') alias: string, @Res() res: Response) {
    if (await this.shortUrlsService.remove(alias)) {
      res.status(HttpStatus.OK).send();
    } else {
      res.status(HttpStatus.NOT_FOUND).json(`${alias} not found`).send();
    }
  }

  private getSortProperty(shortUrl: ShortUrl, sortBy: SortField) {
    switch (sortBy) {
      case SortField.alias:
        return shortUrl.alias;
      case SortField.hits:
        return shortUrl.hits;
      case SortField.url:
        return shortUrl.url;
      case SortField.created:
        return shortUrl.createdAt;
      case SortField.updated:
        return shortUrl.updatedAt;
    }
  }

  private compare(
    a: ShortUrl,
    b: ShortUrl,
    by: SortField,
    order: SortOrder,
  ): number {
    switch (order) {
      case SortOrder.asc:
        return this.getSortProperty(a, by) > this.getSortProperty(b, by)
          ? 1
          : -1;
      case SortOrder.desc:
        return this.getSortProperty(a, by) < this.getSortProperty(b, by)
          ? 1
          : -1;
    }
  }
}
