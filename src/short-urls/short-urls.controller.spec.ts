import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Response } from 'express';
import { ShortUrlsController } from './short-urls.controller';
import { ShortUrlsService } from './short-urls.service';
import { ModuleMocker } from 'jest-mock';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { ShortUrlMapper } from './short-urls.mapper';
import { UpdateShortUrlDto } from './dto/update-short-url.dto';
import { ShortUrl } from './entities/short-url.entity';

const moduleMocker = new ModuleMocker(global);

function newMockResponse() {
  const mockResponse = {
    /* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function */
    redirect(status: number, url: string) {},
    status(status: HttpStatus) {
      return this;
    },
    json(json: string) {
      return this;
    },
    send() {},
  };
  return mockResponse;
}

let count = 0;

function padLeadingZeros(num: number, size: number) {
  let s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}

function newCreatePayload() {
  return new CreateShortUrlDto(
    'http://test.com/',
    `test${padLeadingZeros(++count, 6)}`,
  );
}

function randomInt(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

describe('ShortUrlsController', () => {
  let controller: ShortUrlsController;
  let mockService: ShortUrlsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ShortUrlsController],
    })
      .useMocker((token) => {
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token);
          if (mockMetadata != null) {
            const Mock = moduleMocker.generateFromMetadata(mockMetadata);
            return new Mock();
          }
        }
      })
      .compile();

    controller = module.get<ShortUrlsController>(ShortUrlsController);
    mockService = module.get<ShortUrlsService>(ShortUrlsService);
  });

  it('should be defined', async () => {
    expect(controller).toBeDefined();
  });

  it('should create a ShortUrl', async () => {
    const payload = newCreatePayload();
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'create')
      .mockImplementation(() => Promise.resolve(payload.alias));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.create(payload, mockResponse as Response);

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.CREATED);
    expect(jsonSpy).toHaveBeenCalledWith(payload.alias);
  });

  it('should fail to create a ShortUrl', async () => {
    const payload = newCreatePayload();
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'create')
      .mockImplementation(() => Promise.resolve(undefined));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.create(payload, mockResponse as Response);

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.CONFLICT);
    expect(jsonSpy).toHaveBeenCalledWith(`${payload.alias} already exists`);
  });

  it('should get a ShortUrl', async () => {
    const payload = newCreatePayload();
    const shortUrl = ShortUrlMapper.dtoToEntity(payload);
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'findOne')
      .mockImplementation(() => Promise.resolve(shortUrl));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.findOne(shortUrl.alias, mockResponse as Response);

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.OK);
    expect(jsonSpy).toHaveBeenCalledWith(shortUrl);
  });

  it('should not get a ShortUrl', async () => {
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'findOne')
      .mockImplementation(() => Promise.resolve(undefined));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.findOne('anything', mockResponse as Response);

    expect(jsonSpy).toHaveBeenCalledWith('anything not found');
    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);
  });

  it('should delete a ShortUrl', async () => {
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'remove')
      .mockImplementation(() => Promise.resolve(true));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    await controller.remove('anything', mockResponse as Response);

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.OK);
  });

  it('should not delete a ShortUrl', async () => {
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'remove')
      .mockImplementation(() => Promise.resolve(false));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.remove('anything', mockResponse as Response);

    expect(jsonSpy).toHaveBeenCalledWith('anything not found');
    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);
  });

  it('should update a ShortUrl', async () => {
    const payload = newCreatePayload();
    const updatePayload = new UpdateShortUrlDto({ url: payload.url });
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'update')
      .mockImplementation(() => Promise.resolve(true));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.update(
      payload.alias as string,
      updatePayload,
      mockResponse as Response,
    );

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.OK);
  });

  it('should not update a ShortUrl', async () => {
    const updatePayload = new UpdateShortUrlDto({ url: 'http://anything.com' });
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'update')
      .mockImplementation(() => Promise.resolve(false));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');
    await controller.update(
      'anything',
      updatePayload,
      mockResponse as Response,
    );

    expect(jsonSpy).toHaveBeenCalledWith('anything not found');
    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);
  });

  it('should get empty list of ShortUrl', async () => {
    jest
      .spyOn(mockService, 'findAll')
      .mockImplementation(() => Promise.resolve([]));

    expect(await controller.findAll(undefined, undefined)).toStrictEqual([]);
  });

  it('should get sorted list of ShortUrl', async () => {
    const allList: ShortUrl[] = [];
    const times = randomInt(1, 10);
    for (let i = times; i--; ) {
      allList.push(ShortUrlMapper.dtoToEntity(newCreatePayload()));
    }

    const firstAlias = ShortUrlMapper.dtoToEntity(newCreatePayload());
    firstAlias.alias = 'aaaaa';
    allList.push(firstAlias);
    const lastAlias = ShortUrlMapper.dtoToEntity(newCreatePayload());
    lastAlias.alias = 'zzzzzz';
    allList.push(lastAlias);

    const firstUrl = ShortUrlMapper.dtoToEntity(newCreatePayload());
    firstUrl.url = new URL('http://aaa.com/');
    allList.push(firstUrl);
    const lastUrl = ShortUrlMapper.dtoToEntity(newCreatePayload());
    lastUrl.url = new URL('http://zzz.com/');
    allList.push(lastUrl);

    const firstHits = ShortUrlMapper.dtoToEntity(newCreatePayload());
    firstHits.hits = -1;
    allList.push(firstHits);
    const lastHits = ShortUrlMapper.dtoToEntity(newCreatePayload());
    lastHits.hits = 999;
    allList.push(lastHits);

    const firstCreated = ShortUrlMapper.dtoToEntity(newCreatePayload());
    firstCreated.createdAt = new Date('1970-01-01');
    allList.push(firstCreated);
    const lastCreated = ShortUrlMapper.dtoToEntity(newCreatePayload());
    lastCreated.createdAt = new Date('2970-01-01');
    allList.push(lastCreated);

    const firstUpdated = ShortUrlMapper.dtoToEntity(newCreatePayload());
    firstUpdated.updatedAt = new Date('1970-01-01');
    allList.push(firstUpdated);
    const lastUpdated = ShortUrlMapper.dtoToEntity(newCreatePayload());
    lastUpdated.updatedAt = new Date('2970-01-01');
    allList.push(lastUpdated);

    jest
      .spyOn(mockService, 'findAll')
      .mockImplementation(() => Promise.resolve(allList));

    expect((await controller.findAll('alias', undefined))[0]).toBe(firstAlias);
    expect((await controller.findAll('alias', 'desc'))[0]).toBe(lastAlias);
    expect((await controller.findAll('url', 'asc'))[0]).toBe(firstUrl);
    expect((await controller.findAll('url', 'desc'))[0]).toBe(lastUrl);
    expect((await controller.findAll('hits', 'asc'))[0]).toBe(firstHits);
    expect((await controller.findAll('hits', 'desc'))[0]).toBe(lastHits);
    expect((await controller.findAll('created', 'asc'))[0]).toBe(firstCreated);
    expect((await controller.findAll('created', 'desc'))[0]).toBe(lastCreated);
    expect((await controller.findAll('updated', 'asc'))[0]).toBe(firstUpdated);
    expect((await controller.findAll('updated', 'desc'))[0]).toBe(lastUpdated);
  });
});
