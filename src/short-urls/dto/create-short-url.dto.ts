import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlphanumeric,
  IsLowercase,
  IsOptional,
  IsUrl,
  Length,
} from 'class-validator';

export class CreateShortUrlDto {
  //fields
  @Length(5, 20)
  @IsLowercase()
  @IsAlphanumeric()
  @IsOptional()
  @ApiProperty({ required: false })
  alias?: string;
  @IsUrl()
  @ApiProperty({ example: 'http://example.com/' })
  url: string;

  constructor(url: string, alias?: string) {
    this.url = url;
    this.alias = alias;
  }
}
