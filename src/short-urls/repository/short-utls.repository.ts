import { ShortUrl } from '../entities/short-url.entity';

export abstract class IShortUrlsRepository {
  abstract get(alias: string): Promise<ShortUrl | undefined>;
  abstract save(shortUrl: ShortUrl): Promise<void>;
  abstract delete(alias: string): Promise<boolean>;
  abstract findAll(): Promise<ShortUrl[]>;
}
