import { Injectable } from '@nestjs/common';
import { ShortUrl } from '../entities/short-url.entity';
import { IShortUrlsRepository } from './short-utls.repository';

@Injectable()
export class ShortUrlsInMemRepository implements IShortUrlsRepository {
  store: Map<string, ShortUrl> = new Map();

  constructor() {
    //this is to already start with something
    this.save(
      new ShortUrl(
        'alias1',
        new URL('http://www.google.com'),
        new Date(),
        new Date(),
      ),
    );
    this.save(
      new ShortUrl(
        'alias2',
        new URL('http://www.bing.com'),
        new Date(),
        new Date(),
      ),
    );
    this.save(
      new ShortUrl(
        'alias3',
        new URL('http://www.ecosia.org'),
        new Date(),
        new Date(),
      ),
    );
  }

  async get(alias: string): Promise<ShortUrl | undefined> {
    return this.store.get(alias);
  }

  async save(shortUrl: ShortUrl): Promise<void> {
    this.store.set(shortUrl.alias, shortUrl);
  }

  async delete(alias: string): Promise<boolean> {
    const found = this.store.get(alias);
    if (!found) {
      return false;
    } else {
      this.store.delete(alias);
      return true;
    }
  }

  async findAll(): Promise<ShortUrl[]> {
    return Array.from(this.store.values());
  }
}
