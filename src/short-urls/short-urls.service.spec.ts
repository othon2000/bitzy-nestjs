import { Test, TestingModule } from '@nestjs/testing';
import { CreateShortUrlDto } from './dto/create-short-url.dto';
import { UpdateShortUrlDto } from './dto/update-short-url.dto';
import { ShortUrl } from './entities/short-url.entity';
import { IShortUrlsRepository } from './repository/short-utls.repository';
import { ShortUrlsInMemRepository } from './repository/short-utls.repository.mem';
import { ShortUrlsService } from './short-urls.service';

function randomInt(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

describe('ShortUrlsService', () => {
  let service: ShortUrlsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: IShortUrlsRepository, useClass: ShortUrlsInMemRepository },
        ShortUrlsService,
      ],
    }).compile();

    service = module.get<ShortUrlsService>(ShortUrlsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create ShortUrl with specific alias, and should not re-create it', async () => {
    const alias = 'mytest';
    const dto = new CreateShortUrlDto('http://mytest.com/', alias);
    await service.create(dto);
    expect((await service.findOne(alias))?.alias).toBe('mytest');
    expect(await service.create(dto)).toBeUndefined();
  });

  it('should create ShortUrl without specific alias, and update it', async () => {
    const dto = new CreateShortUrlDto('http://mytest.com/');
    const alias = (await service.create(dto)) as string;
    expect(await service.findOne(alias)).toBeDefined();
    const updateDto = new UpdateShortUrlDto();
    const updatedUrl = 'http://mytest-updated.com/';
    updateDto.url = updatedUrl;
    expect(await service.update(alias, updateDto)).toBe(true);
    expect(await service.findOne(alias)).toBeDefined();
    expect(((await service.findOne(alias)) as ShortUrl).url.toString()).toBe(
      updatedUrl,
    );
  });

  it('should fail to delete inexisting ShortUrl', async () => {
    expect(await service.remove('I_DO_NOT_EXIST')).toBeFalsy();
  });

  it('should fail (return false) to delete inexisting ShortUrl', async () => {
    expect(await service.remove('I_DO_NOT_EXIST')).toBeFalsy();
  });

  it('should fail (return false) to update inexisting ShortUrl', async () => {
    const updateDto = new UpdateShortUrlDto();
    const updatedUrl = 'http://mytest-updated.com/';
    updateDto.url = updatedUrl;
    expect(await service.update('I_DO_NOT_EXIST', updateDto)).toBeFalsy();
  });

  it('should not find inexisting ShortUrl after deleting it', async () => {
    const alias = 'mytest';
    const dto = new CreateShortUrlDto('http://mytest.com/', alias);
    await service.create(dto);
    expect(await service.findOne(alias)).toBeDefined();
    expect(await service.remove(alias)).toBeTruthy();
    expect(await service.findOne(alias)).toBeUndefined();
    expect(await service.findOneAndIncreaseHits(alias)).toBeUndefined();
  });

  it('should increase hits of a ShortUrl', async () => {
    const alias = 'mytest';
    const dto = new CreateShortUrlDto('http://mytest.com/', alias);
    await service.create(dto);
    expect(await service.findOne(alias)).toBeDefined();
    const times = randomInt(1, 999);
    for (let i = times; i--; ) {
      await service.findOneAndIncreaseHits(alias);
    }
    expect(await service.findOne(alias)).toBeDefined();
    expect(((await service.findOne(alias)) as ShortUrl).hits).toBe(times);
  });

  it('should return all ShortUrls', async () => {
    const times = randomInt(1, 999);
    for (let i = times; i--; ) {
      await service.create(
        new CreateShortUrlDto(`http://mytest-${1}.com/`, `alias${i}`),
      );
    }
    expect((await service.findAll()).length).toBe(times);
  });
});
