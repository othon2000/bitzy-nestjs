import { Module } from '@nestjs/common';
import { ShortUrlsModule } from '../short-urls/short-urls.module';
import { RedirectController } from './redirect.controller';

@Module({
  controllers: [RedirectController],
  imports: [ShortUrlsModule],
})
export class RedirectModule {}
