import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import {
  ApiMovedPermanentlyResponse,
  ApiNotFoundResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ShortUrlsService } from '../short-urls/short-urls.service';

@Controller()
export class RedirectController {
  constructor(private readonly shortUrlsService: ShortUrlsService) {}

  @Get(':alias_to_redirect')
  @ApiQuery({ name: 'alias_to_redirect', description: 'the short URL alias' })
  @ApiMovedPermanentlyResponse({ description: 'redirecting to the full URL' })
  @ApiNotFoundResponse({
    description: 'when a short URL with the alias does not exist',
  })
  async redirect(
    @Param('alias_to_redirect') alias: string,
    @Res() res: Response,
  ) {
    const shortUrl = await this.shortUrlsService.findOneAndIncreaseHits(alias);
    if (shortUrl) {
      res.redirect(HttpStatus.MOVED_PERMANENTLY, shortUrl.url.toString());
    } else {
      res.status(HttpStatus.NOT_FOUND).json(`${alias} not found`).send();
    }
  }
}
