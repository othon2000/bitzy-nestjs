import { Test, TestingModule } from '@nestjs/testing';
import { Response } from 'express';
import { ShortUrlsService } from '../short-urls/short-urls.service';
import { RedirectController } from './redirect.controller';
import { ModuleMocker } from 'jest-mock';
import { ShortUrl } from '../short-urls/entities/short-url.entity';
import { HttpStatus } from '@nestjs/common';

const moduleMocker = new ModuleMocker(global);

function newMockResponse() {
  const mockResponse = {
    /* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function */
    redirect(status: number, url: string) {},
    status(status: HttpStatus) {
      return this;
    },
    json(json: string) {
      return this;
    },
    send() {},
  };
  return mockResponse;
}

describe('RedirectController', () => {
  let controller: RedirectController;
  let mockService: ShortUrlsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RedirectController],
    })
      .useMocker((token) => {
        if (typeof token === 'function') {
          const mockMetadata = moduleMocker.getMetadata(token);
          if (mockMetadata != null) {
            const Mock = moduleMocker.generateFromMetadata(mockMetadata);
            return new Mock();
          }
        }
      })
      .compile();

    controller = module.get<RedirectController>(RedirectController);
    mockService = module.get<ShortUrlsService>(ShortUrlsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should redirect when the ShortUrl exists', async () => {
    const mockResponse = newMockResponse();
    const shortUrl = new ShortUrl(
      'alias1',
      new URL('http://alias1.com/'),
      new Date(),
      new Date(),
    );

    jest
      .spyOn(mockService, 'findOneAndIncreaseHits')
      .mockImplementation(() => Promise.resolve(shortUrl));
    const redirectSpy = jest.spyOn(mockResponse, 'redirect');

    await controller.redirect('alias1', mockResponse as Response);

    expect(redirectSpy).toHaveBeenCalledWith(
      HttpStatus.MOVED_PERMANENTLY,
      shortUrl.url.toString(),
    );
  });

  it('should return 404 and error message when the ShortUrl does not exist', async () => {
    const mockResponse = newMockResponse();
    jest
      .spyOn(mockService, 'findOneAndIncreaseHits')
      .mockImplementation(() => Promise.resolve(undefined));

    const statusSpy = jest.spyOn(mockResponse, 'status');
    const jsonSpy = jest.spyOn(mockResponse, 'json');

    await controller.redirect('alias1', mockResponse as Response);
    const errorMessage = `alias1 not found`;

    expect(statusSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);
    expect(jsonSpy).toHaveBeenCalledWith(errorMessage);
  });
});
