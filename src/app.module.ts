import { Module } from '@nestjs/common';
import { ShortUrlsModule } from './short-urls/short-urls.module';
import { RedirectModule } from './redirect/redirect.module';

@Module({
  controllers: [],
  providers: [],
  imports: [ShortUrlsModule, RedirectModule],
})
export class AppModule {}
