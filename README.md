![pipeline](https://gitlab.com/othon2000/bitzy-nestjs/badges/master/pipeline.svg)
![coverage](https://gitlab.com/othon2000/bitzy-nestjs/badges/master/coverage.svg)

## Description

URL shortener project using [Nest](https://github.com/nestjs/nest) framework. The diagram below is a simplified view of the architecture, hiding the components from Nest such as routing, pipes, and filters.

```mermaid
flowchart TD
  InternetUser(["&lt;User&gt;<br>Internet User"])-->|"GET /:alias"|RedirectController
  AdminUser(["&lt;User&gt;<br>URL Creator"])-->|"GET | POST | PATCH | DELETE<br>/admin/short-url/[:alias]"|ShortUrlController
  RedirectController-->|"HTTP 301 -> http://full.url"|InternetUser
  subgraph ShortUrlModule
    ShortUrlController-->|manages|ShortUrlService
    ShortUrlService-->InMemRepository[("InMemRepository<br>Map&lt;string,ShortUrl&gt;")]
  end
  subgraph RedirectModule
    RedirectController-->|"gets and<br>increments hits"|ShortUrlService
  end
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Accessing the Swagger UI

http://localhost:3000/api/

In the UI, you can use the API to manage short URLs.


### Trying a Redirect

For any previously created short URL, just access in the browser: `http://localhost:3000/$alias_name/`.

Example: http://localhost:3000/alias1/ (already exists by default).

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
