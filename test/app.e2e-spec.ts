import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { CreateShortUrlDto } from './../src/short-urls/dto/create-short-url.dto';
import { UpdateShortUrlDto } from './../src/short-urls/dto/update-short-url.dto';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(404);
  });

  const SHORT_URLS = '/admin/short-urls/';
  let count = 0;

  function newCreatePayload() {
    return new CreateShortUrlDto('http://e2e.com/', `e2e${++count}`);
  }

  function randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  it(`${SHORT_URLS} (POST)`, async () => {
    const payload = newCreatePayload();

    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(201, `"${payload.alias}"`);
    //second time, should be conflict
    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(409, `"${payload.alias} already exists"`);

    const payloadWithoutAlias = new CreateShortUrlDto('http://e2e.com/');
    return request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payloadWithoutAlias)
      .expect(201);
  });

  it(`${SHORT_URLS}:alias (PATCH)`, async () => {
    const payload = newCreatePayload();

    //create
    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(201, `"${payload.alias}"`);

    //update
    const updatePayload = new UpdateShortUrlDto();
    updatePayload.url = 'http://updated.com/';
    await request(app.getHttpServer())
      .patch(`${SHORT_URLS}${payload.alias}`)
      .send(updatePayload)
      .expect(200);

    //verify
    await request(app.getHttpServer())
      .get(`${SHORT_URLS}${payload.alias}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('alias', payload.alias);
        expect(res.body).toHaveProperty('url', updatePayload.url);
      });

    //not found
    return request(app.getHttpServer())
      .patch(`${SHORT_URLS}alias_does_not_exist`)
      .send(payload)
      .expect(404);
  });

  it(`${SHORT_URLS}:alias (GET)`, async () => {
    const payload = newCreatePayload();

    //create
    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(201, `"${payload.alias}"`);

    //get
    await request(app.getHttpServer())
      .get(`${SHORT_URLS}${payload.alias}`)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty('alias', payload.alias);
        expect(res.body).toHaveProperty('url', payload.url);
      });

    //not found
    await request(app.getHttpServer())
      .get(`${SHORT_URLS}alias_does_not_exist`)
      .expect(404);
  });

  it(`${SHORT_URLS}:alias (DELETE)`, async () => {
    const payload = newCreatePayload();

    //create
    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(201, `"${payload.alias}"`);

    //delete
    await request(app.getHttpServer())
      .delete(`${SHORT_URLS}${payload.alias}`)
      .expect(200);

    //not found
    await request(app.getHttpServer())
      .delete(`${SHORT_URLS}${payload.alias}`)
      .expect(404);
  });

  it(`${SHORT_URLS} (GET)`, async () => {
    //seed
    const payloads: CreateShortUrlDto[] = [];
    const times = randomInt(1, 999);
    for (let i = times; i--; ) {
      payloads.push(newCreatePayload());
    }
    for (const one of payloads) {
      await request(app.getHttpServer())
        .post(SHORT_URLS)
        .send(one)
        .expect(201, `"${one.alias}"`);
    }

    //get all
    await request(app.getHttpServer())
      .get(`${SHORT_URLS}`)
      .expect(200)
      .then((res) => {
        expect(res.body.length).toBeGreaterThanOrEqual(times);
      });

    //get all sorted by hits
    const index = randomInt(0, times - 1);
    const alias = payloads[index].alias;
    //do one redirect
    await request(app.getHttpServer()).get(`/${alias}`).expect(301);
    //get sorted
    await request(app.getHttpServer())
      .get(`${SHORT_URLS}?sort=hits&order=desc`)
      .expect(200)
      .then((res) => {
        expect(res.body[0].alias).toBe(alias);
      });
  });

  it(`/:alias (GET) (redirect)`, async () => {
    const payload = newCreatePayload();

    //create
    await request(app.getHttpServer())
      .post(SHORT_URLS)
      .send(payload)
      .expect(201, `"${payload.alias}"`);

    //redirect
    await request(app.getHttpServer()).get(`/${payload.alias}`).expect(301);

    //not found
    await request(app.getHttpServer()).get(`/alias_does_not_exist`).expect(404);
  });
});
